package com.tenxcloud.zipkin.demo.controller;

import org.springframework.amqp.core.Message;
import org.springframework.stereotype.Component;

@Component
public class RabbitListener {

    @org.springframework.amqp.rabbit.annotation.RabbitListener(queues = "zipkin-demo1")
  private void handleMessage(Message message){
    String messageBody = new String(message.getBody());
    System.out.println("收到了消息是 : " + messageBody);

  }
}
