package com.tenxcloud.zipkin.demo.controller;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @GetMapping("/hello")
    public String http() throws Exception{
      // 发送http请求
      Request request = new Request.Builder().url("http://barve-client:9875/hi").build();
      OkHttpClient client = new OkHttpClient();
      Response response = client.newCall(request).execute();
      return response.body().string();
    }



}
