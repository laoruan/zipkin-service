package com.tenxcloud.zipkin.demo.config;

import brave.Tracing;
import brave.spring.rabbit.SpringRabbitTracing;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class RabbitConfigure {

    @Bean
    public Tracing tracing() {
        return Tracing.newBuilder()
            .localServiceName("spring-amqp-producer")
            .build();
    }

    @Bean
    public SpringRabbitTracing springRabbitTracing(Tracing tracing) {
        return SpringRabbitTracing.newBuilder(tracing)
            .remoteServiceName("receive-service")
            .build();
    }

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory, SpringRabbitTracing springRabbitTracing) {
        RabbitTemplate rabbitTemplate = springRabbitTracing.newRabbitTemplate(connectionFactory);
        return rabbitTemplate;
    }

    @Bean
    public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory(ConnectionFactory connectionFactory, SpringRabbitTracing springRabbitTracing) {
        return springRabbitTracing.newSimpleRabbitListenerContainerFactory(connectionFactory);
    }
}
