package com.tenxcloud.zipkin.demo.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfig {

    @Bean
    public Queue receiveQueue() {
        return new Queue("zipkin-demo1");
    }

}
