package com.tenxcloud.zipkin.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCloudZipkinApplication {

  public static void main(String[] args) {
    SpringApplication.run(SpringCloudZipkinApplication.class, args);
  }

}
